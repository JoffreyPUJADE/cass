#ifndef DEF_CASS
#define DEF_CASS

#include <ostream>
#include <string>
#include <vector>
#include <unordered_map>
#include <utility>
#include <cstdint>
#include "Option.hpp"

struct ParsedLine
{
	std::string instruction;
	size_t lineNumber;
	std::vector<std::string> args;
	std::string entireCodeLine;
	
	friend std::ostream& operator<<(std::ostream& os, struct ParsedLine src)
	{
		os << "Instruction : " << src.instruction << " ; Line Number : " << src.lineNumber << " ; args : [";
		
		for(size_t i=0;i<src.args.size();++i)
			os << src.args[i] << ((i < (src.args.size() - 1)) ? ", " : "");
		
		os << "] ; Entire Code Line : " << src.entireCodeLine;
	}
};

typedef struct ParsedLine ParsedLine;

class Cass
{
	public:
		Cass(std::string sourceFileName, std::string assembledFileName);
		~Cass();
		
		void assemblyProcess();
	
	private:
		std::string m_sourceFileName;
		std::string m_assembledFileName;
		std::vector<std::pair<std::string, size_t> > m_sourceCode;
		std::unordered_map<std::string, std::uint16_t> m_symbolTable;
		std::vector<ParsedLine> m_parsedCode;
		std::vector<std::uint16_t> m_assembledCode; // Pour stocker les opcodes assemblés
		static std::unordered_map<std::string, int> m_instructions;
		static std::unordered_map<std::string, Option> m_optInstructions;
	
	protected:
		bool isWellFormedLabelLine(std::string labelLine);
		
		void preprocessSource();
		std::pair<std::string, std::vector<std::string> > analyzeLine(std::string line);
		void collectLabels();
		void assemble();
		
		void readSourceFile();
		void writeToBinaryFile();
};

#endif // DEF_CASS