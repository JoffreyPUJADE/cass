#ifndef STRINGPROCESSING_HPP
#define STRINGPROCESSING_HPP

#include <string>

static inline std::string removeSpaceAtBeginning(std::string line)
{
	if(line.length() == 0)
		return "";
	
	while(line.length() > 0 && std::isspace(static_cast<unsigned char>(line[0])))
		line = line.erase(0, 1);
	
	return line;
}

static inline bool containsComment(std::string line) { return line.find_first_of(";") != std::string::npos; }

static inline std::string removeComment(std::string line) { return containsComment(line) ? line.erase(line.find_first_of(";")) : line; }

static inline bool containsOnlySpace(std::string line)
{
	bool contains = true;
	
	for(size_t i=0;i<line.length();++i)
		contains = ((contains) && (std::isspace(static_cast<unsigned char>(line[i]))));
	
	return contains;
}

static inline bool containsSpace(std::string str) { return str.find_first_of(" ") != std::string::npos; }

static inline std::string removeSpace(std::string str)
{
	str.erase(std::remove_if(str.begin(), str.end(), [](char c) { return std::isspace(c); }), str.end());
	return str;
}

// Macros error for "Option" class.
#define ARGS_PASSED_BUT_NOT_EXPECTED -25
#define NO_ARGS_PASSED_BUT_EXPECTED -26
#define TOO_FEW_ARGS_PASSED -27
#define TOO_MANY_ARGS_PASSED -28
#define BAD_FORM_OF_ARGS -29

static inline std::string macroError(int err)
{
	switch(err)
	{
		case ARGS_PASSED_BUT_NOT_EXPECTED :
			return "ARGS_PASSED_BUT_NOT_EXPECTED";
		break;
		
		case NO_ARGS_PASSED_BUT_EXPECTED :
			return "NO_ARGS_PASSED_BUT_EXPECTED";
		break;
		
		case TOO_FEW_ARGS_PASSED :
			return "TOO_FEW_ARGS_PASSED";
		break;
		
		case TOO_MANY_ARGS_PASSED :
			return "TOO_MANY_ARGS_PASSED";
		break;
		
		case BAD_FORM_OF_ARGS :
			return "BAD_FORM_OF_ARGS";
		break;
		
		default:
			return "Success";
		break;
	}
}

#endif // STRINGPROCESSING_HPP