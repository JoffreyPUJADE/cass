#ifndef DEF_OPTION
#define DEF_OPTION

#include <string>

class Option
{
	public:
		Option(std::string commandName, int nbOptions, std::string form, int nbBits);
		~Option();
		
		int isCommandWellFormed(std::string commandLine);
	
	private:
		std::string m_commandName; /*!< Name of the command associated to the option. */
		int m_nbOptions; /*!< Number of the options for the command. */
		std::string m_form; /*!< Form of the option. */
		bool m_containsBits; /*!< Indicate if option contains number. */
		int m_nbBits; /*!< Number of bits of the option, if it contains numbers. */
};

#endif // DEF_OPTION