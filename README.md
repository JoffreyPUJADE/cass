# Cass

[La traduction française de ce fichier est ici.](README_FR.md)

A simple Chip8 Assembler, written in pure C++11 for Windows, Mac and Linux (and other OS which can compile C++11 standard).

# Table of contents

* [Sources](#sources)

# Sources

There's my sources used to realize this assembler :

* [Erik Bryntse's Chip8 documentation](http://devernay.free.fr/hacks/chip8/C8TECH10.HTM)
* [mattmikolay's Chip8 instruction set](https://github.com/mattmikolay/chip-8/wiki/CHIP%E2%80%908-Instruction-Set)