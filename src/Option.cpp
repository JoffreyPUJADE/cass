#include "../include/Option.hpp"

#include <algorithm>
#include <regex>
#include <cctype>
#include "../include/StringProcessing.hpp"

Option::Option(std::string commandName, int nbOptions, std::string form, int nbBits) : m_commandName(commandName), m_nbOptions(nbOptions), m_form(form), m_containsBits(nbBits < 0), m_nbBits(nbBits) {}
Option::~Option() {}

int Option::isCommandWellFormed(std::string commandLine)
{
	commandLine = removeSpaceAtBeginning(commandLine);
	commandLine = commandLine.erase(0, m_commandName.length());
	commandLine = removeSpaceAtBeginning(commandLine);
	
	// Checking number of arguments given to instruction.
	if(commandLine.length() > 0 && m_nbOptions == 0)
		return ARGS_PASSED_BUT_NOT_EXPECTED;
	else if(commandLine.length() == 0 && m_nbOptions > 0)
		return NO_ARGS_PASSED_BUT_EXPECTED;
	
	// Line found here : https://stackoverflow.com/questions/3867890/count-character-occurrences-in-a-string-in-c
	size_t commaCount = std::count_if(commandLine.begin(), commandLine.end(), [](char c){return c ==',';});
	
	if(commaCount + 1 < m_nbOptions)
		return TOO_FEW_ARGS_PASSED;
	else if(commaCount + 1 > m_nbOptions)
		return TOO_MANY_ARGS_PASSED;
	
	// Option format check.
	std::regex reg(m_form.c_str());
	
	if(!std::regex_match(commandLine, reg))
		return BAD_FORM_OF_ARGS;
	
	return 1;
}