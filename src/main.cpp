#include "../include/Inclusions.hpp"
#include <iostream>
#include <string>

void help(char const* name)
{
	std::cout << "SYNOPSIS" << std::endl
			  << "	A simple Chip8 Assembler." << std::endl
			  << "DESCRIPTION" << std::endl
			  << "	" << name << " is a Chip8 Assembler, written in C++, for assembling Chip8 ASM source code to Chip8 ROM." << std::endl
			  << "OPTIONS" << std::endl
			  << "	-h/--help" << std::endl
			  << "		Display this help." << std::endl
			  << "	-hf/--helpfr" << std::endl
			  << "		Display this help but in french." << std::endl
			  << "	-a/--assembly [SOURCE_FILE]" << std::endl
			  << "		Realize the assembly process to transform Chip8 ASM source code to Chip8 ROM." << std::endl;
}

void helpFR(char const* name)
{
	std::cout << "SYNOPSIS" << std::endl
			  << "	Un simple assembleur Chip8." << std::endl
			  << "DESCRIPTION" << std::endl
			  << "	" << name << " est un assembleur pour la Chip8, écrit en C++, pour assembler du code Chip8 écrit en assembleur en une ROM pour la Chip8." << std::endl
			  << "OPTIONS" << std::endl
			  << "	-h/--help" << std::endl
			  << "		Affiche cette aide,  mais en anglais." << std::endl
			  << "	-hf/--helpfr" << std::endl
			  << "		Affiche cette aide." << std::endl
			  << "	-a/--assembly [FICHIER_SOURCE]" << std::endl
			  << "		Assemble le fichier source écrit en assembleur Chip8 en une ROM Chip8." << std::endl;
}

int main(int argc, char** argv)
{
	if(argc == 1)
		help(argv[0]);
	else
	{
		std::string opt1 = argv[1];
		
		if(opt1 == "-h" || opt1 == "--help")
			help(argv[0]);
		else if(opt1 == "-hf" || opt1 == "--helpfr")
			helpFR(argv[0]);
		else if(opt1 == "-a" || opt1 == "--assembly")
		{
			if(argc != 4)
				throw std::runtime_error("ERROR : You must give a source file and a destination file !");
			
			std::string sourceFileName = argv[2];
			std::string destinationFileName = argv[3];
			
			Cass assembler(sourceFileName, destinationFileName);
			
			assembler.assemblyProcess();
		}
	}
	
	return 0;
}