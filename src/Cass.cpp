#include "../include/Cass.hpp"

#include <fstream>
#include <sstream>
#include <regex>
#include <stdexcept>
#include "../include/StringProcessing.hpp"

std::unordered_map<std::string, int> Cass::m_instructions = {
	{"SYS", 0x0000}, // Ignore, utilisé pour compatibilité avec le COSMAC VIP
	{"CLS", 0x00E0}, // Effacer l'écran
	{"RET", 0x00EE}, // Retour à partir d'une sous-routine
	{"JP", 0x1000},  // Sauter à une adresse | Paramètre : addr
	{"CALL", 0x2000}, // Appeler une sous-routine | Paramètre : addr
	{"SE", 0x3000},  // Sauter si VX == NN
	{"SNE", 0x4000}, // Sauter si VX != NN
	{"SE_VV", 0x5000}, // Sauter si VX == VY
	{"LD", 0x6000},   // Charger NN dans VX
	{"ADD", 0x7000},  // Ajouter NN à VX
	{"LD_VV", 0x8000}, // Charger VY dans VX
	{"OR", 0x8001},   // VX = VX OR VY
	{"AND", 0x8002},  // VX = VX AND VY
	{"XOR", 0x8003},  // VX = VX XOR VY
	{"ADD_VV", 0x8004}, // VX += VY (avec flag de retenue, flag de retenue : VF)
	{"SUB", 0x8005},  // VX -= VY (avec flag de retenue, flag de retenue : VF)
	{"SHR", 0x8006},  // VX >>= 1 (déplacement à droite avec flag de retenue, flag de retenue : VF)
	{"SHR_VV", 0x8006},  // VX >>= VY (déplacement à droite avec flag de retenue, flag de retenue : VF)
	{"SUBN", 0x8007}, // VX = VY - VX (avec flag de retenue, flag de retenue : VF)
	{"SHL", 0x800E},  // VX <<= 1 (déplacement à gauche avec flag de retenue, flag de retenue : VF)
	{"SHL_VV", 0x800E},  // VX <<= VY (déplacement à gauche avec flag de retenue, flag de retenue : VF)
	{"SNE_VV", 0x9000}, // Sauter si VX != VY
	{"LD_I", 0xA000},  // Charger I avec une adresse
	{"JP_V0", 0xB000}, // Sauter à adresse + V0
	{"RND", 0xC000},   // VX = nombre aléatoire & NN
	{"DRW", 0xD000}, // Dessiner un sprite à partir de la mémoire
	{"SKP", 0xE09E},  // Sauter si la touche VX est appuyée
	{"SKNP", 0xE0A1}, // Sauter si la touche VX n'est pas appuyée
	{"LD_VDT", 0xF007}, // Charger VX avec le timer de délai
	{"LD_K", 0xF00A},  // Attendre que la touche VX soit appuyée
	{"LD_DTV", 0xF015}, // Charger le timer de son avec VX
	{"LD_STV", 0xF018}, // Charger le timer de délai avec VX
	{"ADD_I", 0xF01E}, // Ajouter VX à I (gestion de débordement)
	{"LD_F", 0xF029},  // Charger I avec l'emplacement du sprite pour VX
	{"LD_B", 0xF033},  // Stocker la représentation BCD de VX en mémoire
	{"LD_IVX", 0xF055}, // Copier de V0 à VX dans la mémoire
	{"LD_VXI", 0xF065}  // Charger de V0 à VX à partir de la mémoire
};

std::unordered_map<std::string, Option> Cass::m_optInstructions
{
	{"SYS", Option("SYS", 0, "", -1)}, // Ignore, utilisé pour compatibilité avec le COSMAC VIP
	{"CLS", Option("CLS", 0, "", -1)}, // Effacer l'écran
	{"RET", Option("RET", 0, "", -1)}, // Retour à partir d'une sous-routine
	{"JP", Option("JP", 1, "^0x[0-9a-fA-F][0-9a-fA-F][0-9a-fA-F]$", 3)}, // Sauter à une adresse | Paramètre : addr
	{"CALL", Option("CALL", 1, "^0x[0-9a-fA-F][0-9a-fA-F][0-9a-fA-F]$", 3)}, // Appeler une sous-routine | Paramètre : addr
	{"SE", Option("SE", 2, "^V[0-9a-fA-F], ?[0-9a-fA-F][0-9a-fA-F]$", 3)}, // Sauter si VX == NN
	{"SNE", Option("SNE", 2, "^V[0-9a-fA-F], ?[0-9a-fA-F][0-9a-fA-F]$", 3)}, // Sauter si VX != NN
	{"SE_VV", Option("SE_VV", 2, "^V[0-9a-fA-F], ?V[0-9a-fA-F]$", 2)}, // Sauter si VX == VY           - 0x5xy0
	{"LD", Option("LD", 2, "^V[0-9a-fA-F], ?[0-9a-fA-F][0-9a-fA-F]$", 3)}, // Charger NN dans VX
	{"ADD", Option("ADD", 2, "^V[0-9a-fA-F], ?[0-9a-fA-F][0-9a-fA-F]$", 3)}, // Ajouter NN à VX
	{"LD_VV", Option("LD_VV", 2, "^V[0-9a-fA-F], ?V[0-9a-fA-F]$", 2)}, // Charger VY dans VX           - 0x8xy0
	{"OR", Option("OR", 2, "^V[0-9a-fA-F], ?V[0-9a-fA-F]$", 2)}, // VX = VX OR VY           - 0x8xy1
	{"AND", Option("AND", 2, "^V[0-9a-fA-F], ?V[0-9a-fA-F]$", 2)}, // VX = VX AND VY           - 0x8xy2
	{"XOR", Option("XOR", 2, "^V[0-9a-fA-F], ?V[0-9a-fA-F]$", 2)}, // VX = VX XOR VY           - 0x8xy3
	{"ADD_VV", Option("ADD_VV", 2, "^V[0-9a-fA-F], ?V[0-9a-fA-F]$", 2)}, // VX += VY (avec flag de retenue, flag de retenue : VF)           - 0x8xy4
	{"SUB", Option("SUB", 2, "^V[0-9a-fA-F], ?V[0-9a-fA-F]$", 2)}, // VX -= VY (avec flag de retenue, flag de retenue : VF)           - 0x8xy5
	{"SHR", Option("SHR", 2, "^V[0-9a-fA-F]$", 1)}, // VX >>= 1 (déplacement à droite avec flag de retenue, flag de retenue : VF)           - 0x8xy6
	{"SHR_VV", Option("SHR_VV", 2, "^V[0-9a-fA-F], ?V[0-9a-fA-F]$", 2)}, // VX >>= VY (déplacement à droite avec flag de retenue, flag de retenue : VF)           - 0x8xy6
	{"SUBN", Option("SUBN", 2, "^V[0-9a-fA-F], ?V[0-9a-fA-F]$", 2)}, // VX = VY - VX (avec flag de retenue, flag de retenue : VF)           - 0x8xy7
	{"SHL", Option("SHL", 2, "^V[0-9a-fA-F]$", 1)}, // VX <<= 1 (déplacement à gauche avec flag de retenue, flag de retenue : VF)           - 0x8xyE
	{"SHL_VV", Option("SHL_VV", 2, "^V[0-9a-fA-F], ?V[0-9a-fA-F]$", 2)}, // VX <<= VY (déplacement à gauche avec flag de retenue, flag de retenue : VF)           - 0x8xyE
	{"SNE_VV", Option("SNE_VV", 2, "^V[0-9a-fA-F], ?V[0-9a-fA-F]$", 2)}, // Sauter si VX != VY           - 0x9xy0
	{"LD_I", Option("LD_I", 1, "^0x[0-9a-fA-F][0-9a-fA-F][0-9a-fA-F]$", 3)}, // Charger I avec une adresse
	{"JP_V0", Option("JP_V0", 1, "^0x[0-9a-fA-F][0-9a-fA-F][0-9a-fA-F]$", 3)}, // Sauter à adresse + V0
	{"RND", Option("RND", 2, "^V[0-9a-fA-F], ?[0-9a-fA-F][0-9a-fA-F]$", 3)}, // VX = nombre aléatoire & NN
	{"DRW", Option("DRW", 3, "^V[0-9a-fA-F], ?V[0-9a-fA-F], ?[0-9a-fA-F]$", 3)}, // Dessiner un sprite à partir de la mémoire
	{"SKP", Option("SKP", 1, "^V[0-9a-fA-F]$", 1)}, // Sauter si la touche VX est appuyée
	{"SKNP", Option("SKNP", 1, "^V[0-9a-fA-F]$", 1)}, // Sauter si la touche VX n'est pas appuyée
	{"LD_VDT", Option("LD_VDT", 1, "^V[0-9a-fA-F]$", 1)}, // Charger le timer de délai avec le registre Vx
    {"LD_K", Option("LD_K", 1, "^V[0-9a-fA-F]$", 1)},  // Attendre une touche et la stocker dans Vx
    {"LD_DTV", Option("LD_DTV", 1, "^V[0-9a-fA-F]$", 1)}, // Charger le timer de son avec le registre Vx
    {"LD_STV", Option("LD_STV", 1, "^V[0-9a-fA-F]$", 1)}, // Charger le timer de délai avec le registre Vx
    {"ADD_I", Option("ADD_I", 1, "^V[0-9a-fA-F]$", 1)}, // Ajouter VX à I (gestion de débordement)
    {"LD_F", Option("LD_F", 1, "^V[0-9a-fA-F]$", 1)}, // Charger I avec l'emplacement du sprite pour VX
    {"LD_B", Option("LD_B", 1, "^V[0-9a-fA-F]$", 1)}, // Stocker la représentation BCD de VX en mémoire
    {"LD_IVX", Option("LD_IVX", 1, "^V[0-9a-fA-F]$", 1)}, // Copier de V0 à VX dans la mémoire
    {"LD_VXI", Option("LD_VXI", 1, "^V[0-9a-fA-F]$", 1)} // Charger de V0 à VX à partir de la mémoire
};

Cass::Cass(std::string sourceFileName, std::string assembledFileName) : m_sourceFileName(sourceFileName), m_assembledFileName(assembledFileName) {}

Cass::~Cass() {}


void Cass::assemblyProcess()
{
	readSourceFile();
	
	preprocessSource();
	collectLabels();
	assemble();
	
	writeToBinaryFile();
}


bool Cass::isWellFormedLabelLine(std::string labelLine)
{
	labelLine = removeSpace(labelLine);
	
	std::regex reg1("^[A-Za-z][A-Za-z0-9]* ?:$");
	std::regex reg2("^0x[0-9a-fA-F][0-9a-fA-F][0-9a-fA-F][0-9a-fA-F]?:$");
	
	return std::regex_match(labelLine, reg1) || std::regex_match(labelLine, reg2);
}


void Cass::preprocessSource()
{
	for(size_t i=0;i<m_sourceCode.size();++i)
	{
		m_sourceCode[i].first = removeComment(m_sourceCode[i].first);
		
		if(containsOnlySpace(m_sourceCode[i].first))
			m_sourceCode[i].first = "";
	}
	
	for(long long int i=static_cast<long long int>(m_sourceCode.size()) - 1;i>=0;--i)
	{
		if(i > 0 && m_sourceCode[i].first == "" && m_sourceCode[(i - 1)].first == "")
			m_sourceCode.erase(m_sourceCode.begin() + i);
	}
}
#include <iostream>
std::pair<std::string, std::vector<std::string> > Cass::analyzeLine(std::string line)
{
	line = removeSpaceAtBeginning(line);
	//std::cout << line << std::endl;
	
	if(line.empty())
		return {"", {}};
	
	if(line.find_first_of(":") != std::string::npos)
		return { "label", { line } };
	
	size_t spacePos = line.find_first_of(" ");
	std::string instruction = (spacePos == std::string::npos) ? line : line.substr(0, spacePos);
	
	std::vector<std::string> args;
	
	if(spacePos != std::string::npos)
	{
		std::string params = line.substr(spacePos + 1);
		std::istringstream iss(params);
		std::string arg;
		while(std::getline(iss, arg, ','))
			args.push_back(removeSpace(arg));
	}
	
	return { instruction, args };
}

void Cass::collectLabels()
{
	std::uint16_t currentAddress = 0x200;
	
	for(size_t i=0;i<m_sourceCode.size();++i)
	{
		std::string line = m_sourceCode[i].first;
		std::pair<std::string, std::vector<std::string> > label = analyzeLine(line);
		
		if(label.first == "label")
		{
			if(isWellFormedLabelLine(line))
			{
				std::string parsingLine = removeSpace(label.second[0]);
				std::string label = parsingLine.substr(0, parsingLine.length() - 1);
				
				m_symbolTable[label] = currentAddress;
			}
			else
			{
				std::ostringstream ossErr;
				
				ossErr << "ERROR : At line " << m_sourceCode[i].second << " :\n" << line << "\nLabel not correctly defined.";
				
				throw std::runtime_error(ossErr.str().c_str());
			}
		}
		else
			currentAddress += 2;
	}
}

void Cass::assemble()
{
	for(size_t i=0;i<m_sourceCode.size();++i)
	{
		std::pair<std::string, std::vector<std::string> > lineTemp = analyzeLine(m_sourceCode[i].first);
		
		if(lineTemp.first != "" && lineTemp.first != "label")
		{
			ParsedLine pl;
			
			pl.instruction = lineTemp.first;
			pl.lineNumber = m_sourceCode[i].second;
			pl.args = lineTemp.second;
			pl.entireCodeLine = m_sourceCode[i].first;
			
			//std::cout << pl << std::endl;
			
			m_parsedCode.push_back(pl);
		}
	}
	
	std::uint16_t currentAddress = 0x200; // Adresse de départ pour Chip-8
	
	for(size_t i=0;i<m_parsedCode.size();++i)
	{
		std::string const& instruction = m_parsedCode[i].instruction;
		size_t const& lineNumber = m_parsedCode[i].lineNumber;
		std::vector<std::string> const& args = m_parsedCode[i].args;
		std::string entireCodeLine = m_parsedCode[i].entireCodeLine;
		
		// Instruction not recognized.
		if(m_optInstructions.find(instruction) == m_optInstructions.end())
		{
			std::ostringstream ossErr;
			ossErr << "ERROR : Line " << lineNumber << " : Instruction \"" << instruction << "\" not recognized.";
			throw std::runtime_error(ossErr.str().c_str());
		}
		
		std::uint16_t opcode = m_instructions[instruction];
		
		if(!args.empty())
		{
			for(size_t j=0;j<args.size();++j)
			{
				if(args[j].front() == 'V')
				{
					int regNum = std::stoi(args[j].substr(1), nullptr, 16);
					opcode |= (regNum << (8 - 4 * j));
				}
				else if(args[j].front() == 0 && args[j][1] == 'x')
				{
					int value = std::stoi(args[j].substr(1), nullptr, 16);
					opcode |= value;
				}
			}
		}
		
		if(instruction == "JP" || instruction == "CALL" || instruction == "LD_I")
		{
			if(!args.empty() && m_symbolTable.find(args[0]) != m_symbolTable.end())
			{
				std::uint16_t address = m_symbolTable[args[0]];
				opcode |= (address & 0x0FFF);
			}
			else if(!args.empty())
			{
				std::ostringstream ossErr;
				ossErr << "ERROR : Line " << lineNumber << " : Label \"" << args[0] << "\" not found ; code line : " << entireCodeLine;
				throw std::runtime_error(ossErr.str().c_str());
			}
		}
		// Rajouter une adresse si aucun label ni aucune adresse ?
		
		m_assembledCode.push_back(opcode);
		currentAddress += 2;
	}
}

void Cass::readSourceFile()
{
	std::ifstream ifs;
	
	ifs.open(m_sourceFileName);
	
	if(!ifs.is_open())
	{
		std::ostringstream ossErr;
		
		ossErr << "ERROR : Impossible to open file \"" << m_sourceFileName << "\" in reading mode.";
		
		throw std::runtime_error(ossErr.str().c_str());
	}
	
	std::string line;
	size_t lineNumber = 0;
	
	while(std::getline(ifs, line))
	{
		++lineNumber;
		m_sourceCode.push_back(std::pair<std::string, size_t>(line, lineNumber));
	}
	
	ifs.close();
	
	/*if(!ifs)
	{
		std::ostringstream ossErr;
		ossErr << "ERROR : Impossible to close file \"" << m_sourceFileName << "\" in reading mode.";
		throw std::runtime_error(ossErr.str().c_str());
	}*/
}

void Cass::writeToBinaryFile()
{
	std::ofstream ofs;
	
	ofs.open(m_assembledFileName, std::ios::binary);
	
	if(!ofs.is_open())
	{
		std::ostringstream ossErr;
		ossErr << "ERROR : Impossible to open file \"" << m_sourceFileName << "\" in writing mode.";
		throw std::runtime_error(ossErr.str().c_str());
	}
	
	for(size_t i=0;i<m_assembledCode.size();++i)
	{
		std::uint16_t opcode = m_assembledCode[i];
		
		ofs.put(static_cast<char>((opcode >> 8) & 0xFF));
		ofs.put(static_cast<char>(opcode & 0xFF));
	}
	
	ofs.close();
	
	if(!ofs)
	{
		std::ostringstream ossErr;
		ossErr << "ERROR : Impossible to close file \"" << m_sourceFileName << "\" in writing mode.";
		throw std::runtime_error(ossErr.str().c_str());
	}
}